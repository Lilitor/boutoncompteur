package com.example.mycounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView view;
    private ImageView button;
    private int count = 0;
    private final int theNumber = 257;
    private final int theResponse = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view = findViewById(R.id.textView);
        button = findViewById(R.id.bouton);
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                count ++;
                if (theResponse == count) {
                    view.setText(R.string.response);
                } else if (theNumber == count) {
                    view.setText(R.string.juste);
                } else {
                    view.setText(String.valueOf(count));
                }
            }
        });

    }
}